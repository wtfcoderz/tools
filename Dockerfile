FROM buildpack-deps:stretch-scm

# DOCKER
ENV DOCKER_VERSION 18.09.0
ENV DOCKER_CHANNEL edge
RUN curl -fSL https://download.docker.com/linux/static/${DOCKER_CHANNEL}/x86_64/docker-${DOCKER_VERSION}.tgz -o docker.tgz \
 && tar -xzvf docker.tgz \
 && mv docker/* /usr/local/bin/ \
 && rmdir docker \
 && rm docker.tgz

# Docker compose
ENV DOCKER_COMPOSE_VERSION 1.23.1
RUN curl -L https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose \
 && chmod +x /usr/local/bin/docker-compose

# Install kubectl and kubernetes things
ENV KUBERNETES_VERSION 1.12.3
RUN curl -L -o /usr/local/bin/kubectl "https://storage.googleapis.com/kubernetes-release/release/v${KUBERNETES_VERSION}/bin/linux/amd64/kubectl" \
    && chmod +x /usr/local/bin/kubectl \
    && kubectl version --client

# Install Helm
ENV HELM_VERSION 2.11.0
RUN curl "https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz" | tar zx \
    && mv linux-amd64/helm /usr/bin/ \
    && helm version --client

# Install docker-app
ENV DOCKER_APP_VERSION 0.6.0
RUN curl -L https://github.com/docker/app/releases/download/v${DOCKER_APP_VERSION}/docker-app-linux.tar.gz | tar zx \
    && mv docker-app-linux /usr/bin/docker-app \
    && docker-app version

# FAAS cli
RUN curl -sSL https://cli.openfaas.com | sh

# clair-scanner
RUN curl -L "https://github.com/arminc/clair-scanner/releases/download/v8/clair-scanner_linux_amd64" > /usr/local/bin/clair-scanner \
    && chmod +x /usr/local/bin/clair-scanner

# Add some tools
RUN apt-get update \
 && apt-get install -y jq \
                       binutils \
                       gettext-base \
                       ash \
                       curl \
                       git \
                       jq \
                       openssh-client \
                       python-pip \
                       rsync \
                       libxml2-utils \
                       wget \
                       zip \
 && rm -rf /var/lib/apt/lists/* \
 && pip install yq
 
# G Cloud
RUN apt-get update -y && apt-get install lsb-release -y && rm -rf /var/lib/apt/lists/* && \
    export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)" && \
    echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
    apt-get update -y && apt-get install google-cloud-sdk -y && rm -rf /var/lib/apt/lists/*

CMD ["bash"]
